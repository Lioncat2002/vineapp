"use client";

import Card from "@/components/card";
import Navbar from "@/components/navbar";

export default function Home() {
  return (
    /* Navbar */
    <div>
      <Navbar />
      <div className="flex flex-col items-center ">
        <Card
          user="Lioncat"
          title="v dark road"
          video="https://files.catbox.moe/worxo7.mp4"
        />
        <Card
          user="Lioncat"
          title="another road"
          video="https://files.catbox.moe/worxo7.mp4"
        />
        <Card
          user="Lioncat"
          title="yeah road"
          video="https://files.catbox.moe/worxo7.mp4"
        />
      </div>
    </div>
  );
}
