export interface CardData {
  user: string;
  title: string;
  video: string;
}

export default function Card(prop: CardData) {
  return (
    <div className="flex flex-col items-start ">
      {/* Avatar */}
      <div className="flex flex-row">
        {/* User pic */}
        <img className="rounded-full" src="https://picsum.photos/50" alt="" />

        <div className="flex justify-between px-4">
          {/* Username*/}
          <div className="text-lg font-bold">{prop.user}</div>
        </div>
      </div>
      {/* Content */}
      <div className="px-12">{prop.title}</div>
      {/* Video */}
      <video src={prop.video} width={200} height={300} controls></video>
      <hr />
    </div>
  );
}
