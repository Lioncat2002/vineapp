import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";
import VideoCallOutlinedIcon from "@mui/icons-material/VideoCallOutlined";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import Person3OutlinedIcon from "@mui/icons-material/Person3Outlined";

export default function Navbar() {
  return (
    <div className="flex flex-row items-center justify-center rounded-lg">
      <div className="px-12 py-4  text-gray-400 hover:bg-slate-100">
        <HomeOutlinedIcon fontSize="large" />
      </div>

      <div className="px-12 py-4  text-gray-400 hover:bg-slate-100 rounded-lg">
        <SearchOutlinedIcon fontSize="large" />
      </div>

      <div className="px-12 py-4  text-gray-400 hover:bg-slate-100 rounded-lg">
        <VideoCallOutlinedIcon fontSize="large" />
      </div>
      <div className="px-12 py-4  text-gray-400 hover:bg-slate-100 rounded-lg">
        <FavoriteBorderOutlinedIcon fontSize="large" />
      </div>
      <div className="px-12 py-4  text-gray-400 hover:bg-slate-100 rounded-lg">
        <Person3OutlinedIcon fontSize="large" />
      </div>
    </div>
  );
}
